#ifndef PT_MATH_H__
#define PT_MATH_H__

#include <cmath>

#include "ptmath/vec.h"
#include "ptmath/norm.h"
#include "ptmath/onb.h"

#include "ptmath/random.h"

#include "ptmath/ray.h"

#include "ptmath/primitive.h"
#include "ptmath/sphere.h"
#include "ptmath/triangle.h"

static constexpr auto ptmath_epsilon = 1e-3;

#endif // PT_MATH_H__
